import http from 'k6/http';

export let options = {
 stages: [
    { duration: "15m", target: 50 },
    { duration: "30m", target: 15  },
    { duration: "15m", target: 95 },
  ]
};

export default function() {

  var res;
  var d = new Date();
  var n = d.getMinutes() -30;
  if (n < 0) n = -n;

  var url = "";

  for (var i=0;i<n*2;i++) {

    if (Math.random() * 100 > 5 + ( (d.getSeconds() - 30) / 10 ) ) {
      url = ("http://joshlambert-11-10-staging-test.35.239.31.187.nip.io/");
    }
    else {
      url = ("http://joshlambert-11-10-staging-test.35.239.31.187.nip.io/error");
    }
    res = http.get(url);

  }

};
