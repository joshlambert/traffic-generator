# Overview

This is a small [k6](https://k6.io) script which can be used to generate traffic against an Auto DevOps review app. 
* It generates traffic against `/` and `/error`, as some of the sample apps for Auto DevOps respond with errors on the `/error` endpoint.
* It has a simplistic method of varying the amount of traffic to generate slightly more realistic graphs, for things like screenshots.

# How to use

1. Install [k6](https://docs.k6.io/docs/installation)
1. Run, for example: `k6 run load.js --insecure-skip-tls-verify`